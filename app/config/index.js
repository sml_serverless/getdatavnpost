require('dotenv').config();

const config = {
  port: process.env.PORT,

  db: {
    endpoint: process.env.AWS_DYNAMODB_ENDPOINT,
    accessKey: process.env.AWS_DYNAMODB_ACCESS_KEY,
    secretKey: process.env.AWS_DYNAMODB_SECRET_KEY,
    region: process.env.AWS_DYNAMODB_REGION
  },

  redis: {
    url: process.env.REDIS_URL
  },

  jwt: {
    secret: process.env.JWT_SECRET,
    expiry: 3600000
  },

  secretKey: process.env.SECRET_KEY,

  vnpost: {
    key: process.env.VNPOST_KEY,
    token: process.env.VNPOST_TOKEN,
    baseURL: process.env.VNPOST_BASEURL
  }
};

module.exports = config;
