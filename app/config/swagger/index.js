const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

module.exports = function(app) {
  const options = {
    swaggerDefinition: {
      info: {
        title: 'BaseCode API',
        version: '0.0.0',
        description: ''
      },
      securityDefinitions: {
        JWT: {
          type: 'apiKey',
          description: 'JWT authorization of an API',
          name: 'Authorization',
          in: 'header'
        }
      }
    },
    apis: ['app/feature/**/*spec.js']
  };

  const swaggerSpec = swaggerJSDoc(options);
  app.get('/api-docs.json', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
  });

  const uiOptions = {
    swaggerUrl: '/api-docs.json'
  };

  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(null, uiOptions));
};
