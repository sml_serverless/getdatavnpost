const validator = schema => {
  return async (req, res, next) => {
    const result = schema.validate(req.body);
    if (result.error) {
      return res.badRequest(result.error);
    }
    return next();
  };
};

module.exports = validator;
