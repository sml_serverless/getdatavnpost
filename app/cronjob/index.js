const schedule = require('node-schedule');
// const vnpostTask = require('app/tasks/vnpost');

/*

        *    *    *    *    *    *
        ┬    ┬    ┬    ┬    ┬    ┬
        │    │    │    │    │    │
        │    │    │    │    │    └ day of week (0 - 7) (0 or 7 is Sun)
        │    │    │    │    └───── month (1 - 12)
        │    │    │    └────────── day of month (1 - 31)
        │    │    └─────────────── hour (0 - 23)
        │    └──────────────────── minute (0 - 59)
        └───────────────────────── second (0 - 59, OPTIONAL)

*/

console.log('Cronjon running ...');

schedule.scheduleJob('*/1 * * * *', () => {
  console.log('___ Cronjob run again ___');
  // vnpostTask.syncData();
});
