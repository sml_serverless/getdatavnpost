const dynamoose = require('dynamoose');
const Schema = dynamoose.Schema;

const DataVNPostSchema = new Schema({
  itemcode: {
    type: String,
    required: true,
    hashKey: true
  },
  Customercode: {
    type: String,
    required: true,
    index: {
      'name': 'CustomercodeIndex',
      'global': true
    }
  },
  status: {
    type: String,
    default: 'CREATED'
  },
  Batchcode: {
    type: String,
    required: false,
    rangeKey: true
  },
  SenderFullname: {
    type: String,
    required: false
  },
  SenderAddress: {
    type: String,
    required: false
  },
  SenderTel: {
    type: String,
    required: false
  },
  ReceiverFullname: {
    type: String,
    required: false
  },
  ReceiverAddress: {
    type: String,
    required: false
  },
  ReceiverTel: {
    type: String,
    required: false
  },
  SendingContent: {
    type: String,
    required: false
  },
  Value: {
    type: Number,
    required: false
  },
  deliveryHistories: {
    type: Array,
    'schema': [{
      'type': Object,
      'schema': {
        'status': {
          'type': String,
          'required': true
        },
        'timestamp': {
          'type': Date,
          'required': true
        },
        'metadata': {
          'type': Object
        }
      }
    }]
  }
});

const model = dynamoose.model('DataVNPost', DataVNPostSchema);

module.exports = model;
