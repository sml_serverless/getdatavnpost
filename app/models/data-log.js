const dynamoose = require('dynamoose');
const { v4: uuidv4 } = require('uuid');

const Schema = dynamoose.Schema;

const DataLogSchema = new Schema({
  id: { type: String, hashKey: true, default: uuidv4 },
  data: { type: Object }
},{
  'timestamps': {
    'createdAt': 'createdAt',
    'updatedAt': null // updatedAt will not be stored as part of the timestamp
  },
  'saveUnknown': true
});

const model = dynamoose.model('DataLog', DataLogSchema);

module.exports = model;
