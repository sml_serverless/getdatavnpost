const vnpostService = require('app/services/vnpost');
const dataLogService = require('app/services/data-log');
const dataVNPostService = require('app/services/data-vnpost');
const Logger = require('app/libs/logger');

const syncData = async () => {
  const sessionId = (new Date()).getTime();
  const logman = new Logger(sessionId);
  const logger = logman.logger;
  try {
    logger.info('vnpost==========start sync data==========');
    const datas = await vnpostService.getData();
    const promises = datas.map(async data => {
      logger.info('data: \n' + JSON.stringify(data));
      await dataLogService.createFormData({ data });
      await dataVNPostService.createFormData(data);
    });
    await Promise.all(promises);
    logger.info('vnpost==========end sync data==========');
  } catch (error) {
    logger.error(error);
  }
};

module.exports = {
  syncData
};
