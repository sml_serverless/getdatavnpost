const dataLogService = require('app/services/data-log');
const dataVNPostService = require('app/services/data-vnpost');

module.exports = {
  create: async (req, res) => {
    const {
      body: data
    } = req;
    await dataLogService.createFormData({ data });
    const dataVNPost = await dataVNPostService.createFormData(data);
    return res.ok(dataVNPost);
  },

  listing: async (req, res) => {
    const { 
      query
    } = req;
    const data = await dataVNPostService.findAll(query);
    return res.ok(data);
  },

  getOne: async (req, res) => {
    return res.ok({});
  },
};
