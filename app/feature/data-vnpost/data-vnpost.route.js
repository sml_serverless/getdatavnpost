const router = require('express').Router();
const { syncMiddleware } = require('app/libs/common');
const validator = require('app/middlewares/validator');
const dataVNPostController = require('./data-vnpost.controller');
const dataVNPostValidation = require('./data-vnpost.validation');

router.post(
  '/',
  syncMiddleware(validator(dataVNPostValidation.createSchema)),
  syncMiddleware(dataVNPostController.create)
);

router.get(
  '/',
  syncMiddleware(dataVNPostController.listing)
);

router.get(
  '/:id/confirm',
  syncMiddleware(dataVNPostController.getOne)
);

module.exports = router;
