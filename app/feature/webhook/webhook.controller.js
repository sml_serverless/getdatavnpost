const eventLogService = require('app/services/event-log');
const dataVNPostService = require('app/services/data-vnpost');

module.exports = {
  handleEventFromProvider: async (req, res) => {
    const {
      body: data
    } = req;
    await eventLogService.createFormData({ data });
    const itemcode = data.itemcode;
    if (!itemcode) throw new Error('badRequest');
    const status = data.status || 'CREATED';
    const timestamp = data.timestamp ? new Date(data.timestamp) : new Date();
    const dataVNPost = await dataVNPostService.update(itemcode, {
      '$SET': { 'status': status },
      '$ADD': { 
        'deliveryHistories': {
          status,
          timestamp,
          metadata: data
        } 
      }
    });
    return res.ok(dataVNPost);
  }
};
