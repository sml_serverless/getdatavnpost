const router = require('express').Router();
const { syncMiddleware } = require('app/libs/common');
// const validator = require('app/middlewares/validator');
const webhookController = require('./webhook.controller');
// const webhookValidation = require('./webhook.validation');

router.post(
  '/vnpost-provider',
  // syncMiddleware(validator(webhookValidation.createSchema)),
  syncMiddleware(webhookController.handleEventFromProvider)
);

module.exports = router;
