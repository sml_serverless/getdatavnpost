const Joi = require('joi');

module.exports = {
  createSchema: Joi.object().keys({
    itemcode: Joi.string().strip().required(),
    Customercode: Joi.string().strip().required(),
    Batchcode: Joi.string().strip().required(),
    SenderFullname: Joi.string().strip().required(),
    SenderAddress: Joi.string().strip().required(),
    SenderTel: Joi.string().strip().required(),
    ReceiverFullname: Joi.string().strip().required(),
    ReceiverAddress: Joi.string().strip().required(),
    ReceiverTel: Joi.string().strip().required(),
    SendingContent: Joi.string().strip().required(),
    Value: Joi.number().integer().required()
  })
};
