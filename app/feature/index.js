const router = require('express').Router();
const dataVNPostRouter = require('./data-vnpost');
const webhookRouter = require('./webhook');

router.use('/data-vnposts', dataVNPostRouter);
router.use('/webhook', webhookRouter);

module.exports = router;
