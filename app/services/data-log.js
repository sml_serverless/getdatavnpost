const DataLog = require('app/models/data-log');

const createFormData = async (data) => {
  const dataLog = await DataLog.create(data);
  return dataLog;
};

const findById = async id => {
  const dataLog = await DataLog.queryOne({ id }).exec();
  return dataLog;
};

const findAll = async () => {
  const dataLogs = await DataLog.scan().exec();
  return dataLogs;
};

module.exports = {
  createFormData,
  findAll,
  findById,
};
