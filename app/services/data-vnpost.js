const DataVNPost = require('app/models/data-vnpost');
const dynamoose = require('dynamoose');

const createFormData = async (data) => {
  const dataVNPost = await DataVNPost.create(data);
  return dataVNPost;
};

const findById = async id => {
  const dataVNPost = await DataVNPost.queryOne({ id }).exec();
  return dataVNPost;
};

const findAll = async (query) => {
  let condition = new dynamoose.Condition();
  for (const [key, value] of Object.entries(query)) {
    condition = condition.where(key).eq(value);
  }
  const dataVNPosts = await DataVNPost.scan(condition).exec();
  return dataVNPosts;
};

const update = async (itemcode, updateObj) => {
  const dataVNPost = await DataVNPost.update({ itemcode }, updateObj );
  return dataVNPost;
};

module.exports = {
  createFormData,
  findAll,
  findById,
  update
};
