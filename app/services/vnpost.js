const { vnpost: vnpostConfig } = require('app/config');
const axios = require('axios').default;
const vnpostClient = axios.create({
  baseURL: vnpostConfig.baseURL,
  headers: {
    key: vnpostConfig.key,
    token: vnpostConfig.token
  }
});

const getData = async () => {
  const vnpostDataFunction = 'Core_GetDataContinuouslyByItem_Kho_Smartlog';
  const res = await vnpostClient.get('/getdata', {
    headers: {
      'data': vnpostDataFunction
    }
  });
  return res.data;
};

module.exports = {
  getData
};