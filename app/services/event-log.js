const EventLog = require('app/models/event-log');

const createFormData = async (data) => {
  const eventLog = await EventLog.create(data);
  return eventLog;
};

const findById = async id => {
  const eventLog = await EventLog.queryOne({ id }).exec();
  return eventLog;
};

const findAll = async () => {
  const eventLogs = await EventLog.scan().exec();
  return eventLogs;
};

module.exports = {
  createFormData,
  findAll,
  findById,
};
