const cryptojs = require('crypto-js');
const { secretKey } = require('app/config');

const hash = text => {
  return cryptojs.SHA256(text, secretKey).toString();
};

const verify = (text, hashed) => {
  return hashed === hash(text);
};

module.exports = {
  hash,
  verify
};
