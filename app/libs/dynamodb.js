const dynamoose = require('dynamoose');
const { db: dbConfig } = require('app/config');

const setup = () => {
  dynamoose.aws.sdk.config.update({
    accessKeyId: dbConfig.accessKey,
    secretAccessKey: dbConfig.secretKey,
    region: dbConfig.region
  });
  if (dbConfig.endpoint != null) {
    dynamoose.local(dbConfig.endpoint);
  }
};

module.exports = {
  setup
};
