const Joi = require('joi');
const {
  user: { role: userRoleOptions }
} = require('app/config');

module.exports = {
  emailSchema: Joi.string()
    .email()
    .max(256)
    .label('Email'),
  passwordSchema: Joi.string()
    .min(6)
    .max(20)
    .label('Password'),
  passwordConfirmSchema: Joi.any()
    .valid(Joi.ref('password'))
    .label('Password confirm'),
  roleSchema: Joi.string()
    .invalid(Object.values(userRoleOptions))
    .label('Role'),
  nameSchema: Joi.string()
    .strip()
    .min(6)
    .max(20)
    .label('Name')
};
