const winston = require('winston');

const transport = new winston.transports.Console();

class Logger {
  constructor(session) {
    this.sessionId = session;
    this.transport = transport;
    this.transport.formatter = options => `${options.timestamp()} -${this.sessionId}- ${options.level}: ${options.message}`;
    this.logger = winston.createLogger({
      transports: [this.transport]
    });
  }
}

module.exports = Logger;