const jwt = require('jsonwebtoken');
const config = require('app/config');
const { client: redisClient } = require('app/libs/redis');
const jwtOptions = {
  expiresIn: '30m'
};

module.exports = {
  getToken: userId => {
    const token = jwt.sign(
      {
        userId
      },
      config.jwt.secret,
      jwtOptions
    );
    return token;
  },
  verifyToken: async token => {
    const logouted =
      (await redisClient.getAsync(token)) == 'logouted' ? true : false;
    if (logouted) throw Error('Token had expired');
    const { userId } = jwt.verify(token, config.jwt.secret);
    return userId;
  },
  expiryToken: async token => {
    await redisClient.setAsync(token, 'logouted', 'EX', config.jwt.expiry);
    return;
  }
};
