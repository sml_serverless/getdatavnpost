const express = require('express');
const http = require('http');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const logger = require('morgan');
const helmet = require('helmet');
const baseResponse = require('app/libs/baseResponse');
const dynamodb = require('app/libs/dynamodb');
const config = require('app/config');
const swagger = require('app/config/swagger');
const cors = require('cors');
const app = express();
const server = http.createServer(app);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(helmet());
app.use(cookieParser());
app.use(bodyParser.json());

app.set('views', 'app/views');
app.set('view engine', 'pug');

app.use(baseResponse);
app.use('/static', express.static('static'));

dynamodb.setup();
swagger(app);
app.use(cors());
app.use('/api', require('app/feature'));

app.use((req, res) => {
  res.notFound('not found');
});

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  console.error(err);
  res.internalServerError('internal server error');
});

try {
  server.listen(config.port);
  console.log(`server listen at ${config.port}`);
} catch (error) {
  console.log(`server listen err ${error}`);
  process.exit(0);
}

require('./app/cronjob');

module.exports = app;
