module.exports = {
  env: {
    node: true,
    commonjs: true,
    es6: true,
  },
  extends: [
    'eslint:recommended',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parser: 'babel-eslint',
  rules: {
    "object-curly-spacing": [2, "always"],
    "strict": 0,
    "quotes": [2, "single", "avoid-escape"],
    "semi": [1, "always"],
    "keyword-spacing": [2, { "before": true, "after": true }],
    "space-infix-ops": 2,
    "spaced-comment": [2, "always"],
    "arrow-spacing": 2,
    "no-console": 0
  },
};
